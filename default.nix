{ pkgs ? import <nixpkgs> { }, mkDerivation, base, hpack, stdenv, text, mtl }:
let neutron = pkgs.haskellPackages.callPackage ../neutron/default.nix { };
in
mkDerivation {
  pname = "hackly";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [ base neutron text mtl];
  libraryToolDepends = [ hpack ];
  executableHaskellDepends = [ base neutron text mtl];
  testHaskellDepends = [ base ];
  preConfigure = "hpack";
  homepage = "https://github.com/githubuser/hackly#readme";
  license = stdenv.lib.licenses.bsd3;
}
