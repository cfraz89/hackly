module Main where

import Neutron
import Data.Semigroup
import qualified Data.Text as T
import Data.Functor
import Data.Maybe
import Control.Monad.State.Class as State
import Data.Typeable
import Network.HTTP.Client
import Data.Aeson
import GHC.Generics
import Control.Monad.IO.Class

main :: IO ()
main = do
  manager <- newManager defaultManagerSettings
  neutron Nothing Browsing $ \hs -> do
    window [#title :~ "Hackly"] $ do
      headerBar [#title :~ "Hackly", #showCloseButton := True] $ do
        entry <- widget SearchEntry [#placeholderText :~ "Search"]
        onEvent entry searchChanged $ \t -> do
          rq <- liftIO $ parseRequest $ "http://hackage.haskell.org/packages/search?terms=" <> T.unpack t
          resp <- liftIO $ httpLbs rq { requestHeaders = [("Accept", "application/json")] } manager 
          let packages = decode @[Package] $ responseBody resp
          put $ Searching (fromMaybe [] packages)
      vBox mempty $ do
        case hs of
          Browsing -> void $ widget WebView mempty
          Searching packages -> mapM_ (\p -> widget LinkButton [#label :~ name p, #uri :~ ("http://hackage.haskell.org/package/" <> name p)]) packages


data HacklyState = Searching [Package] | Browsing
      deriving Typeable

data Package = Package { name :: T.Text }
  deriving (Show, Generic, FromJSON)